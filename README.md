API KEY - 8gah4lm98rJBXxz6nlDkGFjFCrVy4q9a

How to run:
1) composer install
2) php bin/console doctrine:migrations:generate
3) php bin/console doctrine:fixtures:load

API docs:
- /api/scooters                  - POST with (UUID, first_latitude, first_longitude, second_latitude, second_longitude, status) values,
- /api/scooters/{:uuid}          - POST with (scooter UUID, latitude, longitude, time) values.
- /api/scooters/{:uuid}/reserve  - reserve scooter trip (GET with the scooter UUID value),
- /api/scooters/{:uuid}/finish   - finish scooter trip (GET with the scooter UUID value),

- /api/clients/start             - GET fake clients' implementation with UUID,


Fake client process can be started by request 'start' and seen in action by looking at database values.
