<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Location;
use App\Entity\Scooter;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 5; $i++) {
            $client = new Client();
            $client->setUUID('CL' . rand(10000, 90000));
            $client->setName('name' . $i);
            $client->setSurname('surname' . $i);
            $manager->persist($client);
        }

        $scooters = [];
        for ($i = 1; $i < 10; $i++) {
            $scooter = new Scooter();
            $scooter->setUUID('SC' . rand(10000, 90000));
            $scooter->setStatus(true);
            $scooters[] = $scooter;
            $manager->persist($scooter);
        }

        for ($i = 1; $i < 100; $i++) {
            $location = new Location($scooters[rand(0, 8)], rand(10 * 10, 30 * 10) / 10, rand(10 * 10, 30 * 10) / 10, new \DateTime('+' . $i . 'minutes'));
            $manager->persist($location);
        }

        $manager->flush();
    }
}
