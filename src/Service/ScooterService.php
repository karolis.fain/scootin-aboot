<?php

namespace App\Service;

use App\Entity\Scooter;
use App\Repository\LocationRepository;
use App\Repository\ScooterRepository;
use App\ValueObject\Point;
use App\ValueObject\ScooterLocation;
use Doctrine\ORM\ORMException;
use Symfony\Component\Process\Process;

class ScooterService
{
    /**
     * @var ScooterRepository $scooterRepository
     */
    private ScooterRepository $scooterRepository;

    /**
     * @var LocationRepository $locationRepository
     */
    private LocationRepository $locationRepository;

    /**
     * ScooterService constructor.
     * @param ScooterRepository $scooterRepository
     * @param LocationRepository $locationRepository
     */
    public function __construct(ScooterRepository $scooterRepository,
                                LocationRepository $locationRepository)
    {
        $this->scooterRepository = $scooterRepository;
        $this->locationRepository = $locationRepository;
    }

    /**
     * @param string $uuid
     * @return Scooter|null
     */
    public function findByUUID(string $uuid): ?Scooter
    {
        return $this->scooterRepository->findOneBy(['UUID' => $uuid]);
    }

    /**
     * @param Scooter $scooter
     * @throws ORMException
     */
    public function changeStatus(Scooter $scooter): void
    {
        $scooter->setStatus(!$scooter->getStatus());
        $this->scooterRepository->update($scooter);
    }

    /**
     * @param array $requestData
     * @return array|null
     */
    public function getScootersInRectangle(array $requestData): ?array
    {
        $firstPoint = new Point($requestData['first_latitude'], $requestData['first_longitude']);
        $secondPoint = new Point($requestData['second_latitude'], $requestData['second_longitude']);

        return $this->locationRepository
            ->getScootersInRectangle($firstPoint, $secondPoint, $requestData['status']);
    }

    /**
     * @param array $scooters
     * @return ScooterLocation
     */
    public function getRandomScooterLocation(array $scooters): ScooterLocation
    {
        $randomScooter = $scooters[rand(0, count($scooters) - 1)];

        return new ScooterLocation($randomScooter['UUID'], $randomScooter['latitude'],
            $randomScooter['longitude'], $randomScooter['status']);
    }

    public function start(string $rootDir): void
    {
        set_time_limit(500);
        $runningProcesses = [];

        for ($i = 0; $i < 3; $i++) {
            $process = new Process(
                ['php', 'bin/console', 'app:fake']
            );
            $process->setWorkingDirectory($rootDir);
            $process->start();

            $runningProcesses[] = $process;
        }

        while (count($runningProcesses)) {
            foreach ($runningProcesses as $key => $runningProcess) {
                if (!$runningProcess->isRunning()) {
                    unset($runningProcesses[$key]);
                }

                sleep(1);
            }
        }
    }

}