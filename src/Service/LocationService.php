<?php

namespace App\Service;

use App\Entity\Location;
use App\Entity\Scooter;
use App\Repository\LocationRepository;
use Doctrine\ORM\ORMException;

class LocationService
{
    /**
     * @var LocationRepository $locationRepository
     */
    private LocationRepository $locationRepository;

    /**
     * LocationService constructor.
     * @param LocationRepository $locationRepository
     */
    public function __construct(LocationRepository $locationRepository)
    {
        $this->locationRepository = $locationRepository;
    }

    /**
     * @param Scooter $scooter
     * @param array $requestData
     * @return Location
     */
    public function formLocation(Scooter $scooter, array $requestData): Location
    {
        return Location::createFromRequest($scooter, $requestData);
    }

    /**
     * @param Location $location
     * @throws ORMException
     */
    public function updateLocation(Location $location): void
    {
        $this->locationRepository->update($location);
    }

}