<?php

namespace App\Entity;

use App\Repository\LocationRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LocationRepository::class)
 */
class Location
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=Scooter::class, inversedBy="locations")
     * @ORM\JoinColumn(nullable=false)
     */
    private Scooter $scooter;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="decimal", precision=10, scale=8)
     */
    private string $latitude;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="decimal", precision=11, scale=8)
     */
    private string $longitude;

    /**
     * @Assert\DateTime
     * @Assert\NotBlank
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $time;

    /**
     * Location constructor
     * @param Scooter $scooter
     * @param string $latitude
     * @param string $longitude
     * @param DateTimeInterface $time
     */
    public function __construct(Scooter $scooter, string $latitude,
                                string $longitude, DateTimeInterface $time)
    {
        $this->scooter = $scooter;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->time = $time;
    }

    /**
     * @param Scooter $scooter
     * @param array $req
     * @return Location
     */
    public static function createFromRequest(Scooter $scooter, array $req)
    {
        return new Location($scooter, $req['latitude'], $req['longitude'], $req['time']);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScooterId(): Scooter
    {
        return $this->scooter;
    }

    public function setScooterId(Scooter $scooter_id): self
    {
        $this->scooter = $scooter_id;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getTime(): ?DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

}
