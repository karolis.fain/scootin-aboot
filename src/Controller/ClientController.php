<?php

namespace App\Controller;

use Exception;
use App\Service\ScooterService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class ClientController
 * @Route("/api/clients", name="clients")
 * @package App\Controller
 */
class ClientController extends AbstractController
{
    /**
     * @var ScooterService
     */
    private ScooterService $scooterService;

    /**
     * ClientController constructor.
     * @param ScooterService $scooterService
     */
    public function __construct(ScooterService $scooterService)
    {
        $this->scooterService = $scooterService;
    }

    /**
     * @Route("/start", name="start", methods={"GET"})
     * @return Response
     * @throws Exception
     */
    public function start(): Response
    {
        $this->scooterService->start($this->getParameter('kernel.project_dir'));

        return $this->json('Fake api process finished!', 200);
    }
}
