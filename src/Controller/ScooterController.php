<?php

namespace App\Controller;

use App\Service\LocationService;
use App\Service\ScooterService;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ScooterController
 * @Route("/api/scooters", name="scooters")
 * @package App\Controller
 */
class ScooterController extends AbstractController
{
    /**
     * @var LocationService
     */
    private LocationService $locationService;

    /**
     * @var ScooterService
     */
    private ScooterService $scooterService;

    /**
     * ScooterController constructor.
     * @param LocationService $locationService
     * @param ScooterService $scooterService
     */
    public function __construct(LocationService $locationService, ScooterService $scooterService)
    {
        $this->locationService = $locationService;
        $this->scooterService = $scooterService;
    }

    /**
     * @Route("/", name="find", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function find(Request $request): JsonResponse
    {
        $scooters = $this->scooterService->getScootersInRectangle($request->request->all());

        return $this->json($scooters, 200);
    }

    /**
     * @Route("/{uuid}", name="update", methods={"POST"})
     * @param Request $request
     * @param string $uuid
     * @param ValidatorInterface $validator
     * @return JsonResponse
     * @throws ORMException
     */
    public function update(Request $request, string $uuid, ValidatorInterface $validator): JsonResponse
    {
        $scooter = $this->scooterService->findByUUID($uuid);
        $location = $this->locationService->formLocation($scooter, $request->request->all());

        $errors = $validator->validate($location);
        if (count($errors) > 0) {
            return $this->json((string) $errors, 400);
        }

        $this->locationService->updateLocation($location);

        return $this->json('Location updated!', 200);
    }

    /**
     * @Route("/{uuid}/reserve", name="reserve", methods={"GET"})
     * @param string $uuid
     * @return JsonResponse
     * @throws ORMException
     */
    public function reserve(string $uuid): JsonResponse
    {
        $scooter = $this->scooterService->findByUUID($uuid);
        if (!$scooter->getStatus()) {
            return $this->json('Scooter is taken!', 403);
        }

        $this->scooterService->changeStatus($scooter);

        return $this->json('Scooter reserved!', 200);
    }

    /**
     * @Route("/{uuid}/finish", name="finish", methods={"GET"})
     * @param string $uuid
     * @return JsonResponse
     * @throws ORMException
     */
    public function finish(string $uuid): JsonResponse
    {
        $scooter = $this->scooterService->findByUUID($uuid);
        if ($scooter->getStatus()) {
            return $this->json('Scooter is not taken!', 403);
        }

        $this->scooterService->changeStatus($scooter);

        return $this->json('Trip finished!', 200);
    }

}
