<?php

namespace App\ValueObject;

final class Point
{
    /**
     * @var string
     */
    private string $latitude;

    /**
     * @var string
     */
    private string $longitude;

    /**
     * Point constructor.
     * @param string $latitude
     * @param string $longitude
     */
    public function __construct(string $latitude, string $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

}