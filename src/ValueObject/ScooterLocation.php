<?php

namespace App\ValueObject;

final class ScooterLocation
{
    /**
     * @var string
     */
    private string $uuid;

    /**
     * @var int
     */
    private int $latitude;

    /**
     * @var int
     */
    private int $longitude;

    /**
     * @var bool
     */
    private bool $available;

    /**
     * Point constructor.
     * @param string $uuid
     * @param int $latitude
     * @param int $longitude
     * @param bool $available
     */
    public function __construct(string $uuid, int $latitude,
                                int $longitude, bool $available)
    {
        $this->uuid = $uuid;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->available = $available;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return int
     */
    public function getLatitude(): int
    {
        return $this->latitude;
    }

    /**
     * @return int
     */
    public function getLongitude(): int
    {
        return $this->longitude;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->available;
    }

}