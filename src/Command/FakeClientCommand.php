<?php

namespace App\Command;

use App\Entity\Location;
use App\ValueObject\Point;
use Doctrine\ORM\ORMException;
use App\Service\ScooterService;
use App\Repository\LocationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FakeClientCommand extends Command
{
    const DISTANCE = 0.0003;
    const FIVE_MINUTES = 300;
    const MIN_POINT = 10;
    const MAX_POINT = 30;
    const AVAILABLE_SCOOTER = 1;

    /**
     * @var LocationRepository $locationRepository
     */
    private LocationRepository $locationRepository;

    /**
     * @var ScooterService $scooterService
     */
    private ScooterService $scooterService;

    /**
     * FakeClientCommand constructor.
     * @param LocationRepository $locationRepository
     * @param ScooterService $scooterService
     */
    public function __construct(LocationRepository $locationRepository, ScooterService $scooterService)
    {
        $this->locationRepository = $locationRepository;
        $this->scooterService = $scooterService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:fake')
            ->setDescription('Fake api clients');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws ORMException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $timeToRun = microtime(true) + self::FIVE_MINUTES;

        while ($timeToRun >= microtime(true)) {
            if ($scooters = $this->getScootersByPoints()) {
                $randomScooterData = $this->scooterService->getRandomScooterLocation($scooters);

                $scooter = $this->scooterService->findByUUID($randomScooterData->getUuid());
                $this->scooterService->changeStatus($scooter);

                $distance = self::DISTANCE;
                $until = microtime(true) + rand(10, 15);
                while ($until >= microtime(true)) {
                    $location = new Location($scooter, $randomScooterData->getLatitude() + $distance,
                        $randomScooterData->getLongitude(), new \DateTime());

                    sleep(3);
                    $this->locationRepository->update($location);
                    $distance += self::DISTANCE;
                }

                $this->scooterService->changeStatus($scooter);
            }
            sleep(rand(2, 5));
        }
    }

    /**
     * @return array|null
     */
    private function getScootersByPoints(): ?array
    {
        $firstPoint = new Point(rand(self::MIN_POINT, self::MAX_POINT), rand(self::MIN_POINT, self::MAX_POINT));
        $secondPoint = new Point(rand(self::MIN_POINT, self::MAX_POINT), rand(self::MIN_POINT, self::MAX_POINT));

        return $this->locationRepository
            ->getScootersInRectangle($firstPoint, $secondPoint, self::AVAILABLE_SCOOTER);
    }
}