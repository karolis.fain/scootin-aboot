<?php

namespace App\Repository;

use App\Entity\Location;
use App\Entity\Scooter;
use App\ValueObject\Point;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Location::class);
    }

    /**
     * @param Location $location
     * @throws ORMException
     */
    public function update(Location $location): void
    {
        $this->_em->persist($location);
        $this->_em->flush($location);
    }

    /**
     * @param Point $firstPoint
     * @param Point $secondPoint
     * @param bool $status
     * @return array|null
     */
    public function getScootersInRectangle(Point $firstPoint, Point $secondPoint, bool $status): ?array
    {
        return $this->createQueryBuilder('l')
            ->select('s.UUID, l.latitude, l.longitude, s.status')
            ->join(Scooter::class, 's', 'WITH', 's.id = l.scooter')
            ->where('l.latitude BETWEEN :firstLat AND :secondLat OR l.latitude BETWEEN :secondLat AND :firstLat')
            ->andWhere('l.longitude BETWEEN :firstLong AND :secondLong OR l.longitude BETWEEN :secondLong AND :firstLong')
            ->andWhere('s.status = :status')
            ->setParameter('firstLat', $firstPoint->getLatitude())
            ->setParameter('secondLat', $secondPoint->getLatitude())
            ->setParameter('firstLong', $firstPoint->getLongitude())
            ->setParameter('secondLong', $secondPoint->getLongitude())
            ->setParameter('status', $status)
            ->orderBy('l.time')
            ->groupBy('l.scooter')
            ->getQuery()
            ->getResult();
    }

}
